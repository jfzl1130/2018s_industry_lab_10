package ictgradschool.industry.recursion.ex02;

public class Reversing {
    public String reversing(String s){
        if(s.length()<1){
            return "";

        }
        String smaller = s.substring(1);
        String r = reversing(smaller);
        return r + s.charAt(0);
        //return reversing(s.substring(1)) + s.charAt(0);
    }

    public static void main(String[] args) {
        System.out.println(new Reversing().reversing("cat"));
    }
}

